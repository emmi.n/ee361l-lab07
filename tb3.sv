
// Testbench 3
module tb;
  logic [15:0] addr;
  logic [15:0] instr;
  logic clk;
  logic reset;
  
  instrMemory im(addr,instr);
  computer cpu(addr,instr,clk,reset);
  
  initial
    begin
      clk=0;
      forever #1 clk=~clk;
    end
  
  initial
    begin
      $monitor("clk=%b reset=%b addr=%3d X0=%2d X2=%2d X3=%2d",clk,
               reset,addr,
               cpu.regFile[0],
               cpu.regFile[2],
               cpu.regFile[3]
              );
      reset=1;
      #2
      reset=0;
      #50 $finish;
    end
  
endmodule

module instrMemory(
  input logic [15:0] addr,
  output logic [15:0] instr
);
  
  function logic [15:0] rFormat
    (input instrOpcode op,
     input [2:0] regDst,
     input [2:0] regSrc1,
     input [2:0] regSrc2
    );
    rFormat={op, 3'b0, regDst,regSrc2,regSrc1};
  endfunction
  
  function logic [15:0] iFormat
    (input instrOpcode op,
     input [2:0] regDst,
     input [2:0] regSrc,
     input [5:0] immConst
    );
    iFormat={op, immConst, regDst,regSrc};
  endfunction
  
  function logic [15:0] dFormat
    (input instrOpcode op,
     input [2:0] regRegFile,
     input [2:0] regAddr,
     input [5:0] offsetConst
    );
    dFormat={op, offsetConst, regRegFile,regAddr};
  endfunction
  
  function logic [15:0] cbFormat
    (input instrOpcode op,
     input [2:0] regTest,
     input [8:0] offsetConst
    );
    cbFormat={op, offsetConst, regTest};
  endfunction
  
  function logic [15:0] bFormat
    (input instrOpcode op,
     input [11:0] offsetConst
    );
    bFormat={op, offsetConst};
  endfunction
  
  function logic [15:0] nop
    (
    );
    nop = rFormat(opADD,7,7,7);
  endfunction
  
  always_comb
    case(addr[4:1])
      0: instr=iFormat(opADDI,3,7,2);
      1: instr=cbFormat(opCBZ,3,-1);
      2: instr=nop();
      3: instr=nop();
      4: instr=nop();
      5: instr=iFormat(opADDI,0,3,3);
      6: instr=rFormat(opADD,7,0,3);
      7: instr=rFormat(opADD,2,0,0);
      8: instr=rFormat(opADD,0,7,2);
      9: instr=iFormat(opSUBI,3,3,1);
      10: instr=bFormat(opB,-9);
      default: instr=nop();
    endcase
  
endmodule




